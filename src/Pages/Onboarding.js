import axios from "axios";
import React, { useCallback, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Loader from "../components/Loader";
import { postApicall } from "../Network/ApiCall";
import { ADD_USER, BaseUrl } from "../Network/baseUrl";

export default function Onboarding() {
  const navigate = useNavigate();
  const [num, setNum] = useState("");
  const [data, setData] = useState({});
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    let number = localStorage.getItem("number");
    setNum(number);
  }, []);

  const inputHandler = (e) => {
    setData({
      ...data,
      contact: localStorage.getItem("number"),
      [e.target.name]: e.target.value,
    });
  };
  const linkHandler = (e) => {
    setData({
      ...data,
      twitter: e.target.value,
      insta: e.target.value,
      linkedin: e.target.value,
    });
  };



  const sucessSignUpCallback =  useCallback(async  (res) => {
   
        setLoading(false);
        // localStorage.removeItem("number");
        localStorage.setItem("data", JSON.stringify(data));
        localStorage.setItem("userId", JSON.stringify(res.id.user));
        navigate("/whisky");
  });
  
  const errorSignUpCallback =  useCallback(async  (message) => {
    setLoading(false);
  });
  
    const clickHandler =  (e) => {
      e.preventDefault();
      setLoading(true);
      const url = ADD_USER;
     const params = {...data}

     postApicall(url, params, sucessSignUpCallback, errorSignUpCallback)
         
    };

  // const clickHandler = async (e) => {
  //   e.preventDefault();
  //   setLoading(true);
  //   await BaseUrl.post(ADD_USER,{...data} )
  //   .then((res) => {
  //     console.log("userId kya h === ", res.data.id.user._id)
  //       setLoading(false);
  //       localStorage.removeItem("number");
  //       localStorage.setItem("data", JSON.stringify(data));
  //       localStorage.setItem("userId", JSON.stringify(res.data.id.user));
  //       navigate("/whisky");
  //     })
  //     .catch((err) => {
  //       setLoading(false);
  //       console.log(err);
  //     });
  // };
  // const clickHandler = async (e) => {
  //   e.preventDefault();
  //   setLoading(true);
  //   axios
  //     .post("http://localhost:3000/api/user/addUser", {
  //       ...data,
  //     })
  //     .then((res) => {
  //       setLoading(false);
  //       localStorage.removeItem("number");
  //       localStorage.setItem("data", JSON.stringify(data));
  //       localStorage.setItem("userId", JSON.stringify(res.data.id.userId));
  //       navigate("/whisky");
  //     })
  //     .catch((err) => {
  //       setLoading(false);
  //       console.log(err);
  //     });
  // };

  return (
    <form
      onSubmit={clickHandler}
      className="App h-screen w-screen flex flex-col items-center py-20"
    >
      <div className="text-white text-7xl">Details</div>
      <div className="text-white mb-32 w-2/3 text-center">
        We never disclose your personal information publicly.{" "}
      </div>

      <div className="w-screen text-black flex items-center justify-center mb-5">
        <input
          type="text"
          required
          name="name"
          className=" w-2/3 border h-12 text-xl rounded-full text-center"
          placeholder="Name"
          onChange={inputHandler}
          value={data.name}
        ></input>
      </div>

      <div className="w-screen text-black flex items-center justify-center mb-5">
        <input
          type="email"
          required
          name="email"
          className=" w-2/3 border h-12 text-xl rounded-full text-center"
          placeholder="Email"
          value={data.email}
          onChange={inputHandler}
        />
      </div>

      <div className="w-screen text-black flex items-center justify-center mb-5">
        <input
          className=" w-2/3 border h-12 text-xl  rounded-full text-center"
          placeholder="Phone"
          value={num}
          disabled
        />
      </div>

      <div className="w-screen text-white  flex items-center justify-center mb-5">
        <input
          type="text"
          onChange={linkHandler}
          className=" w-2/3 border h-12 text-xl bg-black rounded-full text-center"
          placeholder="@instagram | @twitter | @linkedin"
          required
        />
      </div>

      <div>
        <button className="h-10 hover:font bold hover:text-black hover:bg-white w-32 bg-rose-600 text-xl text-white rounded-full shadow-lg mb-48">
          {loading ? <Loader /> : "Register"}
        </button>
      </div>
    </form>
  );
}
