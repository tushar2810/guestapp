import React from 'react'
import "../GetStarted/style.css"
import { useNavigate } from 'react-router-dom'
import Button from '../../components/Button/Button'

const FasterAccess = () => {
  const navigate = useNavigate()
  const handleClick =()=>{
    navigate("/enjoy-instant")
  }
  return (
    <div className=" h-screen w-screen flex flex-col items-center py-10" >
      <div className=" text-4xl text-center mb-4 px-4">Faster Access with KronusScore</div>
      <div className="mb-2 font-bold">
        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQZP6pAvRf6nrDm8TzUu-J6WniYKqGWPnXVnGeqefiExHZfTiX7N9SSRLiwyqB6z97sDB8&usqp=CAU" alt="" />
      </div>
      <div>
        <ul className="ul_list num_body mb-16">
          <li className="flex items-center my-2 list_numb ">
            <div className="list_number one_list text-center ">
              <span className="text-white ">1 </span>
            </div>
            <div className="ml-4 Actlist "><span>Connect social media account(s)</span></div>
          </li>
          <li className="flex items-center my-2 list_numb ">
            <div className="list_number two_list text-center ">
              <span className="text-white ">2 </span>
            </div>
            <div className="ml-4 Actlist "><span>Get preferred over other wait line</span></div>
          </li>
          <li className="flex items-center my-2 list_numb ">
            <div className="list_number three_list text-center ">
              <span className="text-white ">3 </span>
            </div>
            <div className="ml-4 Actlist "><span>On table escalations, parks and social credibility</span></div>
          </li>
        </ul>
      </div>
      <div>
        <Button btnText="Understood it's importance!" 
        handleClick={handleClick}
        className="p-2 hover:font bold hover:text-black hover:bg-white bg-rose-600 text-white border border-black  w-60 text-base rounded-full mb-2" />
      </div>
    </div>
  )
}

export default FasterAccess
