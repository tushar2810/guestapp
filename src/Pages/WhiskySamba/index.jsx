import React, { useCallback, useEffect, useState } from 'react'
import "./style.css"
import Button from '../../components/Button/Button'
import TopHeader from '../../components/TopHeader'
import { barcounter } from '../../utils/Images'
import { Link, useLocation, useNavigate, useParams } from 'react-router-dom'
import StartRating from '../../utils/StartRating'
import axios from 'axios'
import { getApicall, postApicall } from '../../Network/ApiCall'
import { BUSINESS, BUSINESS1, GET_BUSINESS_DATA, Imageurl } from '../../Network/baseUrl'
import Loader from '../../utils/Loader'
import Footer from '../../components/Footers'

const WhiskyShamba = () => {
  const navigate = useNavigate()
  const [loading, setLoading] = useState(false)
  const [getData, setGetData] = useState({})
  const {id} = useParams()
  let userData = JSON.parse(localStorage.getItem("data"))
  const sucessSignUpCallback = useCallback((response) => {
    console.log("initial values", response.data)
    setGetData(response.data.data)
    setLoading(false);
  });

  const errorSignUpCallback = useCallback((message) => {
    setLoading(false);
  });

  const barData = () => {
    setLoading(true)
    // const params = { id }
    const url = `${BUSINESS1}${id}`
    getApicall(url,  sucessSignUpCallback, errorSignUpCallback)
  }
  useEffect(() => {
    barData();
  }, [])

  const { business_name, address, description, image, price_range,category, bar_open_time, ticket, photo} = getData || {};

console.log("getData" , getData)
  const handleClick = () => {
    navigate(`/bar-list/Home/${id}`)
  }
  const MainPhoto = photo?.replace("uploads\\" , "")

  const backClick = () => {
    navigate(-1)
  }
  console.log("category.l" , category?.length)
  return (
          <div className="flex flex-col items-center py-4 relative" style={{marginTop:"43px"}}>
            <TopHeader username={userData.name} backClick={backClick} />

            {loading ? <Loader /> : <>
            <div className="flex flex-col items-center align py-4">
              <h3 className="font-bold text-3xl">{business_name}</h3>
              <h4 className="bar_address">{address}</h4>
              {console.log("category" , category)}
              {category?.length ? 
              <p  className="p-2 bold w-52 text-xl text-center  rounded-full shadow-lg " > { category[0]?.category_name} </p> : ""
              }
            </div>
            <div className="image_wid">

                    <img src={`${Imageurl}/${photo}`} alt="bar_image" className="rounded-lg " />
                    
           
                {/* <div className="text-center pt-4">Buzz Rating: 4.9/5</div>

                <StartRating /> */}
            </div>
            <p className=" p-2 mt-2 whisky_dis">
              {description}
            </p>
            <p className=" text-center mb-8 mt-4">
              {price_range}
            </p>
            <div className="mb-12">
              <Button btnText="Get Access"
                handleClick={handleClick}
                className="p-2 hover:font bold hover:text-black hover:bg-white w-52 bg-rose-600 text-xl text-white rounded-full shadow-lg mb-2" />
              <p className=" w-5/6 text-sm text-center">'100% Redeemable'{" "} </p>
            </div> 
            
            </> }

            <Footer />
          </div>
      



  )
}

export default WhiskyShamba
