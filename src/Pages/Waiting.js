import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setUserInfo } from "../features/userSlice";
import { db } from "../firebase";
import { Link, useNavigate } from "react-router-dom";
import firebase from "firebase";
import axios from "axios";

export default function Waiting(x) {
  const [check, setCheck] = useState("");
  const navigate = useNavigate("");
  // useEffect(() => {
  //   const user = localStorage.getItem("userId");
  //   if (user == undefined) {
  //     navigate("/");
  //   }
  // }, []);
  useEffect(() => {
    const val = JSON.parse(localStorage.getItem("userId"));
    if (check !== "Approved") {
      setInterval(function () {
        axios
          .get(`https://localhost:3000/api/user/getUser/${val}`)
          .then((res) => {
            setCheck(res.data.status);
          })
          .catch((err) => {});
      }, 5000);
    }

  }, [check]);

  const handleClient = () => {
    firebase
      .firestore()
      .collection("2500")
      .doc(userId)
      .get()
      .then((snapshot) => {
        console.log(snapshot.data());
      })
      .catch((e) => console.log(e));
  };

  const dispatch = useDispatch();
  var name = window.sessionStorage.getItem("name");
  var amount = sessionStorage.getItem("amount");
  var quantity = sessionStorage.getItem("quantity");

  const [clients, setClients] = useState([]);
  const userId = useSelector((state) => state.user.userId);
  const userName = useSelector((state) => state.user.userName);

  const [paymentMadeChecking, setPaymentMadeChecking] = useState("");

  const handlePress = () => {
    console.log(userId, userName);
  };

  return (
    <div
      onClick={() =>
        dispatch(
          setUserInfo({
            userId: x.Id,
            userName: x.identity,
          })
        )
      }
    >
      <div class="App h-screen w-screen flex flex-col items-center p-12">
        <img
          src="https://scontent.fdel1-4.fna.fbcdn.net/v/t1.18169-9/16266017_1079433235500204_461026913445448743_n.jpg?_nc_cat=105&ccb=1-7&_nc_sid=09cbfe&_nc_ohc=ymbl5cNpTJEAX9K9YNw&tn=_2KXvOwjGQNroxAZ&_nc_ht=scontent.fdel1-4.fna&oh=00_AT8ex0FbN2mPloBXG2hp1Ul_TTxJ8E4reLbZiHgVvfqZ7A&oe=63124957"
          alt="Smiley face"
          width="150"
          height="150"
        ></img>
        <div class=" text-4xl text-white p-2 ">
          Thank You For Registering {name}
        </div>
        <div class=" text-white p-2 ">
          Please pay amount at the gate to get access to your unique, one time
          redeemable QR{" "}
        </div>
        {check == "Approved" && (
          <Link to="/qr" className="bg-rose-600 text-white rounded-full p-3">
            Click Here To Access Pass
          </Link>
        )}
        {/* <Link to="/qr" className="bg-rose-600 text-white rounded-full p-3">Click Here To Access Pass</Link> */}
        <div class=" font-bold text-white p-2 ">{name}</div>
        <div class=" font-bold text-white p-2 ">{amount}</div>
        <div class=" font-bold text-white p-2 ">{quantity}</div>
      </div>
    </div>
  );
}
