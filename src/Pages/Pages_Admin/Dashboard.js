import React from 'react'
import BasicCustomers from './components/BasicCustomers'
import ViplineCusomters from './components/ViplineCusomters'

export default function Dashboard() {
  return (
    <div className=" w-screen flex-col flex justify-center">
       
       <div className="" >
        <div className="font-extrabold text-2xl flex place-content-between mt-10 items-center">
          <div className=" ">BASIC Access</div>
          <div className=" italic text-700">2500</div>
        </div>
         <BasicCustomers/>
        </div>
        <div className="">
        <div className="p-4 font-extrabold text-2xl flex place-content-between items-center">
          <div className=" ">VIP Access</div>
          <div className="italic text-700">4000</div>
        </div>
            <ViplineCusomters/>
        </div>
        <div className="m-10">

        </div>
    </div>
  )
}
