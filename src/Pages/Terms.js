import React from 'react'

export default function Terms() {
  return (
    <div className="App w-screen flex flex-col items-center py-10">
        <div className='text-white font-bold text-4xl'>Privacy Policy</div>
        <p className="text-white text-sm w-5/6 mt-5">
        Welcome to our website. This site is maintained as a service to our customers. By using this site, you agree to comply with and be bound by the following terms and conditions of use. Please review these terms and conditions carefully. If you do not agree to these terms and conditions, you should not use this site.
        </p>

        <div className='text-white font-bold text-xl mt-5'>Agreement</div>
        <p className="text-white text-sm w-5/6 mt-2">
        This Term and Conditions agreement ("the "Agreement") specifies the Terms and Conditions for access to and use of [DREAM CANTEEN]([prathamshankar3@gmail.com]) (usekrunos.com) and describe the terms and conditions applicable to your access of and use of the Website. This Agreement may be modified at any time by [DREAM CANTEEN]upon posting of the modified Agreement. Any such modifications shall be effective immediately. You can view the most recent version of these terms at any time at the Website. Each use by you shall constitute and be deemed your unconditional acceptance of this Agreement.        </p>

        <div className='text-white font-bold text-xl mt-5'>Privacy</div>
        <p className="text-white text-sm w-5/6 mt-2">Your visit to our site is also governed by our Privacy Policy. Please review our Privacy Policy at the Website.</p>

        <div className='text-white font-bold text-xl mt-5'>Ownership</div>
        <p className="text-white text-sm w-5/6 mt-2">All content included on this site is and shall continue to be the property of [DREAM CANTEEN] or its content suppliers and is protected under applicable copyright, patent, trademark, and other proprietary rights. Any copying, redistribution, use or publication by you of any such content or any part of the Website is prohibited, except as expressly permitted in this Agreement. Under no circumstances will you acquire any ownership rights or other interest in any content by or through your use of this Website.</p>   

        <div className='text-white font-bold text-xl mt-5'>Intended Audience</div>
        <p className="text-white text-sm w-5/6 mt-2">This site is intended for teenagers and adults only. This site is not intended for any children under the age of 13.</p>

        <div className='text-white font-bold text-xl mt-5'>Trademarks</div>
        <p className="text-white text-sm w-5/6 mt-2">The name [DREAM CANTEEN], logos, domain names, service marks, and any other distinctive brand features are either trademarks or registered trademarks of [DREAM CANTEEN]. Other product and Dream Canteens mentioned on this Website may be trademarks of their respective owners.</p>

        <div className='text-white font-bold text-xl mt-5'>Siteuse</div>
        <p className="text-white text-sm w-5/6 mt-2">DREAM CANTEEN] grants you a limited, revocable, nonexclusive license to use this site solely for your own personal use and not for republication, distribution, assignment, sublicense, sale, preparation of derivative works, or other use. You agree not to copy materials on the site, reverse engineer or break into the site, or use materials, products or services in violation of any law. The use of the Website is at the discretion of [DREAM CANTEEN], and [DREAM CANTEEN] may terminate your use of the Website at any time.
You must establish a profile to use many features on [DREAM CANTEEN] and may be required to enter information such as a username and password, name, date of birth, gender, email address, zip code, and country. You (“User”) are fully responsible for maintaining the confidentiality of the username and password, and for all activities that occur under that username. All users agree to: (a) immediately notify [DREAM CANTEEN] of any unauthorized use of their username or password or any other breach of security, and (b) ensure that they exit from their account at the end of each session. [DREAM CANTEEN] cannot and will not be liable for any loss or damage arising from failure to comply with this Agreement.
</p>

        <div className='text-white font-bold text-xl mt-5'>Endorse with Third Parties</div>
        <p className="text-white text-sm w-5/6 mt-2">The site contains hyperlinks to other Internet sites and services not under the editorial control of [DREAM CANTEEN]. These hyperlinks are not expressed or implied endorsements or approvals by [DREAM CANTEEN]of any products, services or information available from these third-party sites. 
All content from these links or sites is the sole responsibility of those third parties, and [DREAM CANTEEN] is not responsible for the accuracy or details of those third-party sites. Any information published by [DREAM CANTEEN] pertaining to these sites or services is believed to be accurate at the time of publication but should be verified independent of this Website.
[DREAM CANTEEN] MAY RECEIVE A FEE AND/OR OTHER COMPENSATION ON SOME CLICKS OR PURCHASES MADE ON OR LINKED THROUGH THIS WEBSITE, THROUGH AN ARRANGEMENT IT HAS WITH A THIRD PARTY IF YOU (i) CLICK ON CERTAIN LINKS ON OUR SITE, EMAILS OR NEWSLETTERS, OR (ii) PURCHASE A PRODUCT OR SERVICE AFTER CLICKING A LINK. 
[DREAM CANTEEN] does not directly sell or license any of the products related to the third-party websites or links, and [DREAM CANTEEN] disclaims any responsibility for or liability related to them. [DREAM CANTEEN] does not supervise contact between Users and third parties. Information you share with third party websites should be limited consistent with our Privacy Policy, and [DREAM CANTEEN] is not responsible for financial or payment details based on any activity between the User and a third party.
</p>

        <div className='text-white font-bold text-xl mt-5'>Compliance with laws</div>
        <p className="text-white text-sm w-5/6 mt-2">You agree to comply with all applicable laws regarding your use of the Website. You further agreed that information provided by you is truthful and accurate to the best of your knowledge.
Users agree not to: (a) upload, post, email, transmit or otherwise make available any content that is unlawful, harmful, threatening, abusive, harassing, tortious, defamatory, vulgar, obscene, libelous, invasive of another's privacy, hateful, or racially, ethnically or otherwise objectionable; (b) harm minors in any way; (c) impersonate any person or entity, guide or host, or falsely state or otherwise misrepresent your affiliation with a person or entity; (d) forge headers or otherwise manipulate identifiers in order to disguise the origin of any content transmitted through the Website; (e) upload, post or otherwise make available any content that you do not have a right to make available under any law or under contractual or fiduciary relationships (such as inside information, proprietary and confidential information learned or disclosed as part of employment relationships or under nondisclosure agreements); (f) upload, post or otherwise make available any content that infringes any patent, trademark, trade secret, copyright or other proprietary rights of any party; (g) upload, post or otherwise make available any material that contains software viruses or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer software or hardware or telecommunications equipment; (h) interfere with or disrupt the Website or servers or networks connected to the Website.
[DREAM CANTEEN] reserves the right to suspend or delete your account if you fail to comply with any of the provisions of this Agreement.
</p>

        <div className='text-white font-bold text-xl mt-5'>Indemnification</div>
        <p className="text-white text-sm w-5/6 mt-2">You agree to indemnify, defend and hold [DREAM CANTEEN] and our partners, employees, and affiliates, harmless from any liability, loss, claim and expense, including reasonable attorney's fees, related to your violation of this Agreement or use of the Site.</p>

        <div className='text-white font-bold text-xl mt-5'>Discalimer</div>
        <p className="text-white text-sm w-5/6 mt-2">THE INFORMATION ON THIS SITE IS PROVIDED ON AN "AS IS," "AS AVAILABLE" BASIS. YOU AGREE THAT USE OF THIS SITE IS AT YOUR SOLE RISK. [DREAM CANTEEN] DISCLAIMS ALL WARRANTIES OF ANY KIND, INCLUDING BUT NOT LIMITED TO ANY EXPRESS WARRANTIES, STATUTORY WARRANTIES, AND ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT. TO THE EXTENT YOUR JURISDICTION DOES NOT ALLOW LIMITATIONS ON WARRANTIES, THIS LIMITATION MAY NOT APPLY TO YOU. YOUR SOLE AND EXCLUSIVE REMEDY RELATING TO YOUR USE OF THE SITE SHALL BE TO DISCONTINUE USING THE SITE.</p>

        <div className='text-white font-bold text-xl mt-5'>Limitation of liability</div>
        <p className="text-white text-sm w-5/6 mt-2">UNDER NO CIRCUMSTANCES WILL [DREAM CANTEEN] BE LIABLE OR RESPONSIBLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL (INCLUDING DAMAGES FROM LOSS OF BUSINESS, LOST PROFITS, LITIGATION, OR THE LIKE), SPECIAL, EXEMPLARY, PUNITIVE, OR OTHER DAMAGES, UNDER ANY LEGAL THEORY, ARISING OUT OF OR IN ANY WAY RELATING TO THE SITE, YOUR SITE USE, OR THE CONTENT, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. THIS INCLUDES ANY POTENTIAL DAMAGES ARISING FROM CONTACT WITH OTHER PARTIES THROUGH THIS SITE. YOUR SOLE REMEDY FOR DISSATISFACTION WITH THE SITE AND/OR CONTENT IS TO CEASE ALL OF YOUR SITE USE.
You may have additional rights under certain laws (including consumer laws) which do not allow the exclusion of implied warranties, or the exclusion or limitation of certain damages. If these laws apply to you, the exclusions or limitations in this Agreement that directly conflict with such laws may not apply to you.
</p>

        <div className='text-white font-bold text-xl mt-5'>Use of information</div>
        <p className="text-white text-sm w-5/6 mt-2">[DREAM CANTEEN]reserves the right, and you authorize us, to use and assign all information regarding site uses by you and all information provided by you in any manner consistent with our Privacy Policy.</p>

        <div className='text-white font-bold text-xl mt-5'>Copyrights & Copyright agent</div>
        <p className="text-white text-sm w-5/6 mt-2">If you believe your work has been copied in a way that constitutes copyright infringement, or your intellectual property rights have otherwise been violated, please provide a notice containing all of the following information via email to [email address here] 
(a) An electronic or physical signature of the person authorized to act on behalf of the owner of the copyright or other intellectual property interest;
(b) A description of the copyrighted work that you claim has been infringed;
(c) A description of where the material that you claim is infringing is located on the Site;
(d) Your address, telephone number, and e-mail address;
(e) A statement by you that you have a good faith belief that the disputed use is not authorized by the copyright owner, its agent, or the law; and
(f) A statement by you, made under penalty of perjury, that the above information in your notice is accurate and that you are the copyright owner or authorized to act on the copyright owner's behalf.
</p>

        <div className='text-white font-bold text-xl mt-5'>Applicable LAw</div>
        <p className="text-white text-sm w-5/6 mt-2">You agree that the laws of the [INSERT JURISDICTION HERE], without regard to conflicts of laws provisions, will govern these Terms and Condition and any dispute that may arise between you and [DREAM CANTEEN] or its affiliates. You also agree that the courts of [INSERT JURISDICTION HERE] will have sole and exclusive jurisdiction to hear or determine any dispute or controversy arising under or concerning this Agreement.</p>

        <div className='text-white font-bold text-xl mt-5'>Severability</div>
        <p className="text-white text-sm w-5/6 mt-2">If any provision of this Agreement shall be adjudged by any court of competent jurisdiction to be unenforceable or invalid, that provision shall be limited or eliminated to the minimum extent necessary so that this Agreement will otherwise remain in full force and effect.</p>

        <div className='text-white font-bold text-xl mt-5'>Waiver</div>
        <p className="text-white text-sm w-5/6 mt-2">The failure of [DREAM CANTEEN] to exercise or enforce any right or provision of this Agreement shall not operate as a waiver of such right or provision. Any waiver of this Agreement by [DREAM CANTEEN] must be in writing and signed by an authorized representative of [DREAM CANTEEN].</p>

        <div className='text-white font-bold text-xl mt-5'>Termination</div>
        <p className="text-white text-sm w-5/6 mt-2">. [DREAM CANTEEN] may terminate this Agreement at any time, with or without notice, for any reason.</p>

        <div className='text-white font-bold text-xl mt-5'>Relationship of the parties</div>
        <p className="text-white text-sm w-5/6 mt-2"> Nothing contained in this Agreement or your use of the Site shall be construed to constitute either party as a partner, joint venture, employee or agent of the other party, nor shall either party hold itself out as such. Neither party has any right or authority to incur, assume or create, in writing or otherwise, any warranty, liability or other obligation of any kind, express or implied, in the name of or on behalf of the other party, it being intended by both parties that each shall remain independent contractors responsible for its own actions.</p>

        <div className='text-white font-bold text-xl mt-5'>Entire Agreement</div>
        <p className="text-white text-sm w-5/6 mt-2">This Terms and Conditions constitutes the entire agreement between you and [DREAM CANTEEN] and governs your use of the Site, and supersedes all prior or contemporaneous communications and proposals, whether electronic, oral or written, between you and [DREAM CANTEEN] with respect to this Site. Notwithstanding the foregoing, you may also be subject to additional terms and conditions, posted policies (including but not limited to the Privacy Policy), guidelines, or rules that may apply when you use the Website. [DREAM CANTEEN] may revise this Terms and Conditions at any time by updating this Agreement and posting it on the Site. Accordingly, you should visit the Site and review the Terms and Conditions periodically to determine if any changes have been made. Your continued use of the Website after any changes have been made to the Terms and Conditions signifies and confirms your acceptance of any such changes or amendments to the Terms and Conditions.</p>

    </div>
  )
}
