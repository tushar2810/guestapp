import React from 'react'
import { useNavigate } from 'react-router-dom'
import Button from '../../../components/Button/Button'

const WaitingPage = () => {
  const Navigate = useNavigate()
  const onClick =()=>{
    Navigate("/qr-page")
  }
  const Ticketname = localStorage.getItem("ticketName")
  return (
    <div className="flex flex-col items-center justify-center px-8 py-8 h-screen">
      <div className="h-2/5">
        <p className=" font-bold text-3xl">
          Thank you for applying to {Ticketname}
        </p>
        <p className="">
          Please make payment at the gate to get access to your dynamic wallet
        </p>
        
      </div>
      <div className="flex flex-col items-center justify-center h-3/5">
        {/* <Button className="bg-white w-60 mt-4 h-10 border border-black rounded-3xl" btnText="Connect Whisky Samba" /> */}
        <Button className="bg-rose-600 w-60 mt-4 text-white h-10 rounded-3xl" btnText="Access my wallet" handleClick={onClick} />
        
      </div>
    </div>
  )
}

export default WaitingPage