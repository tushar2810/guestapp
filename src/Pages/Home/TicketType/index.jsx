<<<<<<< HEAD
import React from 'react'
import { useLocation, useNavigate } from 'react-router-dom'
import Button from '../../../components/Button/Button'
import { Imageurl } from '../../../Network/baseUrl'
import "./style.css"

const TicketType = () => {
  const navigate = useNavigate()
  const { state } = useLocation()
  console.log("state", state)
  const { standing_at_bar, seating_at_bar, standing, seating, title, icon, photo } = state;
  const MainPhoto = photo?.replace("uploads\\", "")
  const handleTable = (value) => {
    navigate("/ticket-dis", { state : {
      state,
      value
    }})

  }
  return (
    <div className="ticket_bg" style={{
      backgroundImage: `url(${Imageurl}/${MainPhoto})`
    }}>
      <div className="bg-rose-600 py-12 px-2 bottom_style">
        <p className="text-center font-bold text-white text-4xl mb-8">{title}</p>
        <div className="ticket_list">

          {
            seating_at_bar &&
            <div className="ticket_value" onClick={() => handleTable(seating)}>
              <div className="headingText">
                <p className="p-4 font-bold text-center">Seating Table for 2</p>
              </div>
              <div className="headingImg" >
                {/* <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSn7EJZQur90yzJiOQYUPUkYwG0isvIQrem3LDOeYI&s" alt="" /> */}
                <img src={icon} alt="" />
                <p className="p-4 font-bold text-center">{seating}</p>

              </div>
            </div>
          }

          {
            standing_at_bar &&
            <div className="ticket_value" onClick={() => handleTable(standing)}>

              <div className="headingText">
                <p className="p-4 font-bold text-center">Standing at Bar</p>
              </div>
              <div className="headingImg">
                {/* <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSn7EJZQur90yzJiOQYUPUkYwG0isvIQrem3LDOeYI&s" alt="" /> */}
                <img src={icon} alt="" />
                <p className="p-4 font-bold text-center">{standing}</p>

              </div>
            </div>
          }
        </div>
      </div>

    </div>
  )
}

=======
import React from 'react'
import { useLocation, useNavigate } from 'react-router-dom'
import Button from '../../../components/Button/Button'
import { Imageurl } from '../../../Network/baseUrl'
import "./style.css"

const TicketType = () => {
  const navigate = useNavigate()
  const { state } = useLocation()
  console.log("state", state)
  const { standing_at_bar, seating_at_bar, standing, seating, title, icon, photo } = state;
  console.log("icon", title)
  const MainPhoto = photo?.replace("uploads\\", "")
  console.log("state", state)
  const handleTable = (value) => {
    navigate("/ticket-dis", { state : {
      state,
      value
    }})

  }
  return (
    <div className="ticket_bg" style={{
      backgroundImage: `url(${Imageurl}/${photo})`
    }}>
      <div className="bg-rose-600 py-12 px-2 bottom_style">
        <p className="text-center font-bold text-white text-4xl mb-8">{title}</p>
        <div className="ticket_list">

          {
            seating_at_bar &&
            <div className="ticket_value" onClick={() => handleTable(seating)}>
              <div className="headingText">
                <p className="p-4 font-bold text-center">Seating Table for 2</p>
              </div>
              <div className="headingImg" >
                {/* <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSn7EJZQur90yzJiOQYUPUkYwG0isvIQrem3LDOeYI&s" alt="" /> */}
                <img src={icon} alt="" />
                <p className="p-4 font-bold text-center">{seating}</p>

              </div>
            </div>
          }

          {
            standing_at_bar &&
            <div className="ticket_value" onClick={() => handleTable(standing)}>

              <div className="headingText">
                <p className="p-4 font-bold text-center">Standing at Bar</p>
              </div>
              <div className="headingImg">
                {/* <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSn7EJZQur90yzJiOQYUPUkYwG0isvIQrem3LDOeYI&s" alt="" /> */}
                <img src={icon} alt="" />
                <p className="p-4 font-bold text-center">{standing}</p>

              </div>
            </div>
          }
        </div>
      </div>

    </div>
  )
}

>>>>>>> 244b62fb4838cf119baf4ac50e4016bf672f019f
export default TicketType