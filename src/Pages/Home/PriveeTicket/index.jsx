import React, { useCallback, useEffect, useState } from 'react'
import "./style.css"
import Footer from '../../../components/Footers'
import TopHeader from '../../../components/TopHeader'
import { QRCode } from '../../../utils/Images'
import { useLocation } from 'react-router-dom'
import { getApicall } from "../../../Network/ApiCall"
import { Imageurl, qrCodeUrl } from '../../../Network/baseUrl'
import Loader from '../../../components/Loader'
const PriveeTicket = () => {

  const UserData = JSON.parse(localStorage.getItem("data"))
  const { state } = useLocation()
  const { business_name, price, date, buy_admission, paymentType, bookingId, _id } = state
  const MainDate = date.slice(0, 10)
  const MainTime = date.slice(11, 19)
  const [apiRes, setapiRes] = useState("")
  const [loading, setLoading] = useState(false)

  let ticket_name = localStorage.getItem("ticketName")
  const sucessCallback = useCallback((res) => {
    console.log("res yyyy", res.data)
ticket_name=res.data.ticket_name
    setapiRes(res.data.QrData[0].booking_details[0].qr)
    setLoading(false)
  })
  console.log("apiRes", apiRes)
  const errorCallback = useCallback((message) => {
    console.log("messagwe", message)
  })
  useEffect(() => {
    const url = `${qrCodeUrl}${bookingId}`
    setLoading(true)
    getApicall(url, sucessCallback, errorCallback)
  }, [])

  // let Main = apiRes || {}
  // const MainPhoto = Main.path.replace("/uploads/", "")
  return (

    <div className="flex flex-col items-center  py-4 h-screen privee_body" style={{ marginTop: "43px" }}>
      <TopHeader username={UserData.name} />
      <div className="cont_name bg-black w-9/12 text-center mt-4">
        <p className="privee_sm_blog text-sm w-36">{ticket_name}</p>
        <p className="font-bold text-2xl text-white">{business_name}</p>
        <p className="lite_text">Show this wallet page upon payment</p>
        <div className="white_circle top_r_circle"></div>
        <div className="white_circle top_l_circle"></div>
      </div>


      <div className="center_body bg-black prive_mid w-9/12">
        <div className="guestM w-11/12 h-8 text-white text-center rounded-2xl border border-white mt-2">
          {buy_admission} Guests
        </div>
        <div className="w-full flex flex-col items-center justify-between mt-2">
          <div className="privee_date_time mb-2">
            <div className="px-4">
              <p className="text_privee_date text-xs">Date</p>
              <p className="text-white text-xs">{MainDate}</p>
            </div>
            <div className="px-4">
              <p className="text_privee_date text-xs" style={{ minWidth: "66px" }}>Time</p>
              <p className="text-white text-xs">{MainTime}</p>
            </div>
          </div>
          <div className="privee_date_time">
            <div className="px-4">
              <p className="text_privee_date text-xs">Paid</p>
              <p className="text-white text-xs">{price}</p>
            </div>
            <div className="px-4">
              <p className="text_privee_date text-xs" style={{ minWidth: "66px" }}>Methods</p>
              <p className="text-white text-xs">{paymentType}</p>
            </div>
          </div>



        </div>
      </div>


      <div className=" qr_cont flex bg-black prive_bottom w-9/12 rounded-bl-2xl rounded-br-2xl">
        <div className="white_circle btm_r_circle">
        </div>
        <div className="white_circle btm_l_circle"></div>

        {apiRes?.length ? apiRes?.map((item) => {
          const MainPhoto = item.path.replace("/uploads/", "")
          if (item.qrStatus === "Approved") {
            return (
              <>
                <img className='Qr_Code' src={`${Imageurl}${item.path}`} alt="" />
                <p className='QRName'>{item.id}</p>
              </>
            )
          } else {
            return (
              <p className='QRName'>Your booking is cancelled</p>
            )
          }

        }) : <div className='Notapprove'> Booking is not approved yet</div>}


        {/* {loading ? <Loader /> :

          (apiRes ? <img className='Qr_Code' src={`http://localhost:8000/${MainPhoto}`} alt="" /> : "Booking is not approved")} */}
        {/* <img className='Qr_Code' src={`http://localhost:8000/${MainPhoto}`} alt="" /> */}
        {/* <img className='Qr_Code' src={`${Imageurl}${MainPhoto}`} alt="" /> */}

        {/* <button className='Qrbtn'>first btn</button>
        <button className='Qrbtn'>Seconf btn</button> */}

      </div>

      <Footer />
    </div>
  )
}

export default PriveeTicket


