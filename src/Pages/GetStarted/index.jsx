import React from 'react'
import { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import Button from '../../components/Button/Button'
import "./style.css"

const GetStarted = () => {
  const navigate = useNavigate()
  const approve = localStorage.getItem("approved")
  useEffect(()=>{
    if(approve){
      navigate("/bar-list")
    }
  })
  const handleClick=()=>{
    navigate("/faster-access")
  }
  return (
    <div className=" h-screen w-screen flex flex-col items-center py-10" >
      <div className=" text-4xl text-center mb-4 px-4">Get Started</div>
      <div className="mb-2 font-bold">
        <img className="sticker" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQZP6pAvRf6nrDm8TzUu-J6WniYKqGWPnXVnGeqefiExHZfTiX7N9SSRLiwyqB6z97sDB8&usqp=CAU" alt="" />
      </div>
      <div>
        <ul className="ul_list num_body mb-24">
          <li className="flex items-center my-2 list_numb">
            <div className="list_number one_list text-center text-white ">
              1 
            </div>
            <div className="ml-4 Actlist"><span>Create your account in 30s</span></div>
          </li>
          <li className="flex items-center my-2 list_numb ">
            <div className="list_number two_list text-center text-white  ">
            2 
            </div>
            <div className="ml-4 Actlist"><span>Choose your preferred ticket & no. of guests</span></div>
          </li>
          <li className="flex items-center my-2 list_numb ">
            <div className="list_number three_list text-center text-white  ">
             3 
            </div>
            <div className="ml-4 Actlist"><span>Get QR, and redeem during billing</span></div>
          </li>
        </ul>
      </div>
      <div>
        <Button btnText="Okay, next..." 
        handleClick={handleClick}
        className="p-2 bg-rose-600 hover:font bold hover:text-black hover:bg-white w-60 border border-black text-white text-base rounded-full mb-2" />
      </div>
    </div>
  )
}

export default GetStarted
