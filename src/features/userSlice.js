import { createSlice } from '@reduxjs/toolkit'

export const userSlice = createSlice({
    name: "user",
    initialState: {
        userId: null,
        userName: null,
        userDetail: {},
    },

    reducers: {
        setUserInfo: (state, action) => {
            state.userId = action.payload.userId
            state.userName = action.payload.userName
        },
        userDetails: (state, action) => {
            state.userDetail = action.payload
        }
    },
})

export const { setUserInfo, userDetails } = userSlice.actions;

export default userSlice.reducer;