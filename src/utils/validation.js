
import * as Yup from 'yup';








  export const MobileNo_Schema = Yup.object().shape({
    num: Yup.string().min(10, "Phone Number must be at least 10 characters").required("This field is required")
  });

  // ** ** Otp validation schema  ** ** //
  export const Otp_Schema = Yup.object().shape({
    code: Yup.string().required("This field is required")
    
  });



export const Detail_Schema = Yup.object().shape({
  name: Yup.string().required("This field is required"),
  // mobileNo: Yup.string().min(14, Texts.Formik_Invalid_Phone_number).required(Texts.AboutYou_mobileno),
  email: Yup.string().email().required("This field is required"),
  date:Yup.date().required("This field is required")

});


//     // ** ** Email Otp validation schema  ** ** //
//     export const EmailVerify_Schema = Yup.object().shape({
//       emailOtp: Yup.string().min(4,Texts.Formik_Invalid_Otp).required(Texts.Otp_message)
      
//     });




