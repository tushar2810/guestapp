import backTo from "../assets/backTo.png";
import Bar from "../assets/Bar.jpeg";
import home from "../assets/icons8-home-384.png"
import homeDark from "../assets/home.png"
import settings from "../assets/icons8-settings-480.png"
import topic from "../assets/icons8-topic-90.png"
import phone from "../assets/icons8-phone-100.png"
import barcode from "../assets/icons8-barcode-scanner-65.png"
import glass from "../assets/glass-png-15613.png"
import pixBar from "../assets/pexels-photo.jpeg"
import Search from "../assets/icons8-search-60.png"
import Barcode from "../assets/Qrcode.png"
import Wallet from "../assets/wallet.png"
import WalletWhite from "../assets/wallet-white.png"


export const backImage = backTo;
export const barcounter = Bar;
export const HomeIcon = home;
export const HomeDarkIcon = homeDark;
export const settingsIcon = settings;
export const barcodeIcon = barcode;
export const topicIcon = topic;
export const phoneIcon = phone;
export const glassImg = glass;
export const barImage = pixBar;
export const SearchIcon = Search;
export const QRCode = Barcode;
export const WalletIcon = Wallet;
export const WalletWhiteIcon = WalletWhite;