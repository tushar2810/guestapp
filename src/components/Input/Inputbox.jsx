import React from 'react'
import './style.css'

const Inputbox = ({ className, placeholder, value, handleChange, disable, type, name, autoComplete }) => {
    return (

        <input
            autoComplete={autoComplete}
            name={name}
            type={type}
            className={className}
            placeholder={placeholder}
            onChange={handleChange}
            disabled={disable}
            value={value} />
    )
}

export default Inputbox