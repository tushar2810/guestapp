import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";

function ProtectedLayout({ auth, children }) {
  const navigate = useNavigate();

  useEffect(() => {
    const auth = localStorage.getItem("auth");
    if (!auth) {
      navigate("/");
    }
  }, []);

  return <div>{children}</div>;
}

export default ProtectedLayout;
