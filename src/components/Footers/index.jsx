import React from 'react'
import "./style.css"
import { barcodeIcon, HomeDarkIcon, HomeIcon, settingsIcon, WalletIcon, WalletWhiteIcon } from '../../utils/Images'
import {  NavLink, useLocation, useNavigate } from 'react-router-dom'

const Footer = () => {
  const barID =localStorage.getItem("barID")
  const location = useLocation()
  console.log(location.pathname)
  const navigate = useNavigate()
  return (
    <div className="footer_body">
      <div className="icon_size ">
        <NavLink to={`/bar-list`} className={(navData) => (navData.isActive ? "activeLink" : "NotActivelink")}> <span><img src={location.pathname===`/bar-list` || location.pathname === `/bar-list/whisky-samba/${barID}` || location.pathname === `/bar-list/Home/${barID}` ? HomeIcon : HomeDarkIcon} alt="" /></span> </NavLink>
      </div>
      <div className="icon_size">
      <NavLink to="/qr-page" className={(navData) => (navData.isActive ? "activeLink" : "NotActivelink")}> <span><img src={location.pathname==="/qr-page" || location.pathname === "/qr-page/privee-page" ? WalletWhiteIcon : WalletIcon} alt="" /></span> </NavLink>   
           {/* <img src={WalletIcon} onClick={()=>{navigate("/qr-page")}} alt="" /> */}
      </div>
      <div className="icon_size">
        <img src={settingsIcon} alt="icon_size"/>
      </div>

    </div>
  )
}

export default Footer
