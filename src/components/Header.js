import React from "react";
import { useNavigate } from "react-router-dom";

function Header() {
  const navigate = useNavigate();

  const logoutHandler = () => {
    // localStorage.clear();
    navigate("/");
  };
  return (
    <div className="flex items-center justify-end w-full p-4">
      <button
        className="p-2 w-32 hover:font-bold hover:text-black hover:bg-white bg-rose-600 text-xl text-white rounded-full shadow-lg mb-3"
        onClick={logoutHandler}
      >
        Logout
      </button>
    </div>
  );
}

export default Header;
